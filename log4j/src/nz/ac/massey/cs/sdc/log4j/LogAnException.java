package nz.ac.massey.cs.sdc.log4j;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

/**
 * Log an exception.
 * @author jens dietrich
 */
public class LogAnException {

	public static void main(String[] args) throws Exception {
		
		// configure logging
		BasicConfigurator.configure();
		
		// create a logger named "foo"
		Logger logger = Logger.getLogger("Foo");
		
		try {
			doSomethingDangerous();
			logger.debug("file deleted!");
		}
		catch (Exception x) {
			logger.error("deleting file failed", x);
		}


	}
	public static void doSomethingDangerous() throws IOException {
		new File("//\\").createNewFile();
	}

}
