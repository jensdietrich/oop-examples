package nz.ac.massey.cs.sdc.log4j;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * Simple log application, the configuration is read from a configuration file.
 * @author jens dietrich
 */
public class LogItWithConfigFile {

	public static void main(String[] args) throws Exception {
		
		// configure logging
		PropertyConfigurator.configure("log4j.config");
		
		// create a logger named "foo"
		Logger logger = Logger.getLogger("Foo");
		
		// log something
		logger.debug("Hello World");
		
		// log something else
		logger.warn("it's me");
		
		

	}

}
