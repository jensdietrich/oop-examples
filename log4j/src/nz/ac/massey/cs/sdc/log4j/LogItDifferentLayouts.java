package nz.ac.massey.cs.sdc.log4j;

import org.apache.log4j.Appender;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Logger;

/**
 * Simple log application.
 * @author jens dietrich
 *
 */
public class LogItDifferentLayouts {

	public static void main(String[] args) throws Exception {
		
		// configure logging
		BasicConfigurator.configure();
		
		
		// create a logger named "foo"
		Logger logger = Logger.getLogger("Foo");
		Logger rootLogger = logger.getRootLogger();
		
		Appender appender =  (Appender)rootLogger.getAllAppenders().nextElement();
		
		// log something
		logger.debug("Hello World");
		
		// set another layout
		appender.setLayout(new org.apache.log4j.HTMLLayout());
		
		// log something else
		logger.warn("it's me");
		
		// change layout again, and log even more
		appender.setLayout(new org.apache.log4j.PatternLayout("%p [@ %d{dd MMM yyyy HH:mm:ss} in %t] %m%n"));
		logger.info("the layout has changed again");
		
		

	}

}
