
### About ###

Examples to show the use of various design patterns in XML parsing. All classes are executable and process input XML data in different ways, see the comments in the source code for details. 

The script `xmlcasestudy.CreateLargeXMLFile` must be run first to create the (large) input data file required by some of the scripts. 