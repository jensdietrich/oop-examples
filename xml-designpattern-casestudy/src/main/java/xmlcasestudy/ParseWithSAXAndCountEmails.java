package xmlcasestudy;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;

/**
 * Count the emails in a large XML file using a SAX parser.
 * @author jens dietrich
 */
public class ParseWithSAXAndCountEmails {

    static class EmailCounter extends DefaultHandler {
        public int count = 0;
        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            if (qName.equals("email")) {
                count = count+1;
            }
        }
    }

    public static void main(String[] args) throws Exception {
        File xml = new File("emails.xml");

        long t1 = System.currentTimeMillis();

        SAXParserFactory parserFactory = SAXParserFactory.newInstance();
        SAXParser saxParser = parserFactory.newSAXParser();
        EmailCounter counter = new EmailCounter();
        saxParser.parse(xml,counter);

        long t2 = System.currentTimeMillis();

        System.out.println("Email count is " + counter.count);
        System.out.println(".. this took " + (t2-t1) + " ms");

    }
}
