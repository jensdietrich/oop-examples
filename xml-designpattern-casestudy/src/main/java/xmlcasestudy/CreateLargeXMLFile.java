package xmlcasestudy;

import org.apache.commons.lang3.RandomStringUtils;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Random;

/**
 * Utility to create a large XML file with random data for experiments.
 * @author jens dietrich
 */
public class CreateLargeXMLFile {

    public static final int SIZE = 500_000;   // 1,000,000 creates a ca 1 GB file
    public static final Random random = new Random();

    public static void main(String[] args) throws Exception {

        File xml = new File("emails.xml");

        try (PrintWriter out = new PrintWriter(new FileWriter(xml))) {
            out.println("<?xml version=\"1.0\"?>");
            out.println("<emails>");


            for (int i=0;i<SIZE;i++) {
                out.println("\t<email id=\"" + (i+1) + "\">");
                for (int j=0;j<random.nextInt(5);j++) {
                    out.println("\t\t<to>");
                    out.println("\t\t\t<emailaddress>" + randomEmail() + "</emailaddress>" );
                    out.println("\t\t\t<displayname>" + randomName() + "</displayname>" );
                    out.println("\t\t</to>");
                }
                for (int j=0;j<random.nextInt(5);j++) {
                    out.println("\t\t<cc>");
                    out.println("\t\t\t<emailaddress>" + randomEmail() + "</emailaddress>" );
                    out.println("\t\t\t<displayname>" + randomName() + "</displayname>" );
                    out.println("\t\t</cc>");
                }
                for (int j=0;j<random.nextInt(5);j++) {
                    out.println("\t\t<bcc>");
                    out.println("\t\t\t<emailaddress>" + randomEmail() + "</emailaddress>" );
                    out.println("\t\t\t<displayname>" + randomName() + "</displayname>" );
                    out.println("\t\t</bcc>");
                }

                // put in special from to be queried later
                if (i==(SIZE/2)) {
                    out.println("\t\t<from>");
                    out.println("\t\t\t<emailaddress>jens@server.com</emailaddress>");
                    out.println("\t\t\t<displayname>Jens Dietrich</displayname>");
                    out.println("\t\t</from>");
                }
                else {
                    out.println("\t\t<from>");
                    out.println("\t\t\t<emailaddress>" + randomEmail() + "</emailaddress>");
                    out.println("\t\t\t<displayname>" + randomName() + "</displayname>");
                    out.println("\t\t</from>");
                }

                out.println("\t\t<subject>" + randomSubject() + "</subject>" );
                out.println("\t\t<body>");
                out.println(randomBody());
                out.println("\t\t</body>");
                out.println("\t</email>");
            }

            out.println("</emails>");
        }

    }

    private static String randomSubject() {
        return  RandomStringUtils.random(30,true,false);
    }
    private static String randomBody() {
        StringBuilder b = new StringBuilder();
        for (int i=0;i<random.nextInt(30);i++) {
            b.append("\t\t\t");
            b.append(RandomStringUtils.random(80,true,false));
            b.append("\n");
        }
        return  b.toString();
    }

    private static String randomEmail() {
        return  RandomStringUtils.random(5,true,false) + "." +
                RandomStringUtils.random(5,true,false) + "@" +
                RandomStringUtils.random(5,true,false) + ".com";
    }

    private static String randomName() {
        return  RandomStringUtils.random(5,true,false) + " " +
                RandomStringUtils.random(5,true,false);
    }

}
