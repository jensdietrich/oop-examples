package xmlcasestudy;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;


/**
 * Count the emails in a large XML file using the DOM representation.
 * @author jens dietrich
 */
public class ParseWithDOMAndCountEmails {

    public static void main(String[] args) throws Exception {
        File xml = new File("emails.xml");

        long t1 = System.currentTimeMillis();

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document document = db.parse(xml);

        Element root = document.getDocumentElement();
        assert root.getLocalName().equals("emails");
        NodeList children = root.getElementsByTagName("email");

        long t2 = System.currentTimeMillis();

        System.out.println("Email count is " + children.getLength());
        System.out.println(".. this took " + (t2-t1) + " ms");

    }
}
