package xmlcasestudy;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;

/**
 * Count the emails in a large XML file using the DOM representation.
 * @author jens dietrich
 */
public class ParseWithDOMAndFindEmail {

    public static void main(String[] args) throws Exception {
        File xml = new File("emails-small.xml");

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document document = db.parse(xml);

        Element root = document.getDocumentElement();
        assert root.getLocalName().equals("emails");
        NodeList children = root.getElementsByTagName("email");
        for (int i=0;i<children.getLength();i++) {
            Element emailElement = (Element)children.item(i);

            Element fromElement = (Element)emailElement
                .getElementsByTagName("from")
                .item(0);

            Element addressElement = (Element)fromElement
                .getElementsByTagName("emailaddress")
                .item(0);

            String address = addressElement.getTextContent();
            if (address.equals("jens.dietrich@sample.com")) {
                System.out.println("Email found, id is: " + emailElement.getAttribute("id"));
            }
        }
    }
}
