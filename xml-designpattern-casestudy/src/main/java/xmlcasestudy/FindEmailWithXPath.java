package xmlcasestudy;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import static javax.xml.xpath.XPathConstants.*;
import javax.xml.xpath.XPathFactory;
import java.io.File;

/**
 * Find a particular email using xpath.
 * @author jens dietrich
 */
public class FindEmailWithXPath {

    public static void main(String[] args) throws Exception {
        File xml = new File("emails-small.xml");

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbf.newDocumentBuilder();
        Document document = db.parse(xml);

        XPath xpath = XPathFactory.newInstance().newXPath();
        String expression = "/emails/email[from/emailaddress='jens.dietrich@sample.com']/@id";
        NodeList elements = (NodeList) xpath.evaluate(expression, document,NODESET);

        for (int i=0;i<elements.getLength();i++) {
            Attr attribute = (Attr)elements.item(i);
            System.out.println("Email found, id is: " + attribute.getValue());
        }
    }
}
