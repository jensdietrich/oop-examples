package xmlcasestudy;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import static javax.xml.stream.XMLStreamConstants.START_ELEMENT;


/**
 * Count the emails in a large XML file using a StAX parser.
 * @author jens dietrich
 */
public class ParseWithStAXAndCountEmails {

    static class EmailCounter extends DefaultHandler {

        int count = 0;

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            if (qName.equals("email")) {
                count = count+1;
            }
        }
    }

    public static void main(String[] args) throws Exception {

        Thread.sleep(10_000);

        File xml = new File("emails.xml");

        long t1 = System.currentTimeMillis();

        XMLInputFactory f = XMLInputFactory.newInstance();
        Reader reader = new FileReader(xml);
        XMLStreamReader streamReader = f.createXMLStreamReader(reader);

        int count = 0;
        while(streamReader.hasNext()) {
            if (START_ELEMENT == streamReader.next()) {
                if (streamReader.getLocalName().equals("email")) {
                    count = count + 1;
                }
            }
        }

        long t2 = System.currentTimeMillis();

        System.out.println("Email count is " + count);
        System.out.println(".. this took " + (t2-t1) + " ms");

    }
}
