package nz.ac.massey.cs.asdc.multidispatch;

public class C {
	public void call(A a) {
		System.out.println("calling method with A");
	}
	public void call(B b) {
		System.out.println("calling method with B");
	}
}
