package nz.ac.massey.cs.asdc.multidispatch;

public class RunExample {

	public static void main(String[] args) {
		A a = new B();
		new C().call(a);
	}

}
