package aop2;

public class Main2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		StringBuffer b = new StringBuffer();
		doSomething1(b);
		doSomething2(b);
		doSomething3(b);
		
		System.out.println(b);
	}

	static void doSomething1(StringBuffer b) {
		b.append(true);
	}
	
	static void doSomething2(StringBuffer b) {
		b.append(42);
	}
	
	static void doSomething3(StringBuffer b) {
		b.append("hello");
		
	}
}
