package aop3;


public aspect CallAspect {

	pointcut pc(StringBuffer obj,Object param) : (call(StringBuffer StringBuffer.append(String)) || call(StringBuffer StringBuffer.append(boolean))) && target(obj) && args(param);
	
    after(StringBuffer obj,Object param) returning : pc(obj,param) { 
        System.out.println("aspect 3: method invoked! object: " + obj + ", param: " + param + ", param type: " + param.getClass()); 
    } 
}
