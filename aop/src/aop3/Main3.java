package aop3;

public class Main3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Main3 inst = new Main3();
		StringBuffer b = new StringBuffer();
		inst.doSomething1(b);
		inst.doSomething2(b);
		inst.doSomething3(b);
		
		System.out.println(b);
	}

	 void doSomething1(StringBuffer b) {
		b.append(true);
	}
	 void doSomething2(StringBuffer b) {
		b.append(42);
	}
	
	 void doSomething3(StringBuffer b) {
		b.append("hi");
		
	}
}
