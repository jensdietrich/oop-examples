package aop1;

public aspect LogMethodAccess {

	     pointcut log() : execution(void Main1.main(String[])); 

	     after() returning() : log() { 
	         System.out.println("aspect 1:  main() method invoked!"); 
	     } 

	     
}
