package osgi.ds.servicedefs;

import java.util.Date;

public interface DateFormatter {
	public String toString(Date d);
}
