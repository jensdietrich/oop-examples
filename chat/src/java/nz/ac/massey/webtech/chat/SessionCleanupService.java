
package nz.ac.massey.webtech.chat;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * Service to unregister sessions from the Conversation Manager.
 * @author jens dietrich
 */
public class SessionCleanupService implements HttpSessionListener {

    public void sessionCreated(HttpSessionEvent se) {
        //nothing to do here
    }

    public void sessionDestroyed(HttpSessionEvent se) {
        HttpSession session = se.getSession();
        ConversationManager.disconnect(session);
    }
}
