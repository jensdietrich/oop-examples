
package nz.ac.massey.webtech.chat;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Join a conversation. This is a controller, instead of creating content the request is forwarded.
 * @author jens dietrich
 */
public class JoinConversation extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String user = request.getParameter("user");
        String conversation = request.getParameter("conversation");
        ConversationManager.joinConversation(request.getSession(), conversation, user);
        
        // delegate request to actual chat ! 
        this.getServletContext().getRequestDispatcher("/chat.jsp").forward(request, response);
    }

    @Override
    public String getServletInfo() {
        return "join a conversation";
    }
}
