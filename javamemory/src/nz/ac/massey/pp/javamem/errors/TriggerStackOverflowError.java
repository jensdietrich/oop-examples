package nz.ac.massey.pp.javamem.errors;

import java.util.*;

/**
 * Example to illustrate want causes stack overflow errors
 * @author jens dietrich
 */
public class TriggerStackOverflowError {

	
	public static void main(String[] args) {
		foo();
	}
	
	// recursion (methods calling themselves) is basically ok and often useful
	// at each step, information about the method invocation has to be stored on a stack
	// however, here the recursion never terminate - this will force to JVM to raise 
	// a stack overflow error and exit
	static void foo() {
		foo();
	}
	
	// this is an example of "good" recursion
	public static int factorial (int i) {
		return i==0?1:factorial(i-1)*i;
	}

}
