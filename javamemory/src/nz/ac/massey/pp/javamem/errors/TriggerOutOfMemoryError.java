package nz.ac.massey.pp.javamem.errors;

import java.util.*;

/**
 * Example to illustrate want causes out of memory errors
 * @author jens dietrich
 */
public class TriggerOutOfMemoryError {

	
	public static void main(String[] args) {
		int counter = 0;
		List list = new ArrayList();
		// keeps adding new strings to a list - each string needs heap memory space,
		// and the garbage collector can not free heap space as the objects are referenced 
		// in the list - this will force to JVM to raise an out of memory error and exit
		while (true) {
			list.add("object"+(counter++));
		}
	}

}
