package nz.ac.massey.pp.javamem.gc;

import java.util.List;

/**
 * A simple object that has a back reference to the list is is an element of.
 * Used to show how garbage collection deals with reference cycles.
 * @author jens dietrich
 */
public class ObjectInList {
	
	public List container = null;

}
