package refleaking1;

public class RunExample {

	public static void main(String[] args) {
		Student student = new Student(42,"John","Smith");
		Address address1 = new Address("Palmerston North","Main Street",42);
		student.setAddress(address1);
		Address address2 = student.getAddress();
		address2.setStreet("Broadway");
		System.out.println("address1.getStreet(): " + address1.getStreet()); 
	}

}
