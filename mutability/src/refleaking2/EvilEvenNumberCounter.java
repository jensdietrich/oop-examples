package refleaking2;

import java.util.List;

public class EvilEvenNumberCounter {
	public static int countEvenNumbers(List<Integer> list) {
		int c = 0;
		for (int i:list) {
			if (i%2==0) c=c+1;
		}
		// do something evil here
		list.clear();
		return c;
	}
}
