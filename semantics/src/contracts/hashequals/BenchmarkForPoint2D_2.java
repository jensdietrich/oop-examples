package contracts.hashequals;

import java.util.*;

/**
 * Simple script to measure insert/lookup performance of a map using keys from Point2D_2.
 * @author jens dietrich
 */
public class BenchmarkForPoint2D_2 {


	public static void main(String[] args) {
		int MAX = 500;
		System.out.println("Measuring map performance with " + (MAX*MAX) + " inserts/lookups");
		
		Map <Point2D_2,String> map = new HashMap <Point2D_2,String>();
		
		long startTime = System.currentTimeMillis();
		// insert
		for (int i=0;i<MAX;i++) {
			for (int j=0;j<MAX;j++) {
				Point2D_2 point = new Point2D_2(i,j);
				map.put(point,"p("+i+","+j+")");
			}
		}
		long insertFinishedTime = System.currentTimeMillis();
		System.out.println("Inserts into map took " + (insertFinishedTime-startTime) + " ms");
		
		// lookup
		for (int i=0;i<MAX;i++) {
			for (int j=0;j<MAX;j++) {
				Point2D_2 point = new Point2D_2(i,j);
				map.get(point);
			}
		}
		long finishedTime = System.currentTimeMillis();
		System.out.println("Lookups took " + (finishedTime-insertFinishedTime) + " ms");
	}

}
