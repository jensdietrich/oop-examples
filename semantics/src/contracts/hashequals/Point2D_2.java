package contracts.hashequals;

/**
 * A correct but not very efficient implementation of hashCode is implemented here
 * @author Jens Dietrich 
 * @version 1.0
 */

public class Point2D_2 extends Point2D {

	public Point2D_2(int x, int y) {
		super(x, y);
	}

	@Override
	public int hashCode() {
		return x%10;
	}
	

}
