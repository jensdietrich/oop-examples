package contracts.hashequals;

/**
 * Simple domain class. Has two coordinates - x and y - and two points are equal if their x and y
 * coordinates are the same. hashCode is implemented in subclasses. This is usually not done like this 
 * it is just a convenient way to experiment with several alternative implementations of hashCode(). 
 * @author Jens Dietrich 
 * @version 1.0
 */

public class Point2D {
	
	protected int y;
	protected int x;


	public Point2D(int x, int y) {
		super();
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final Point2D other = (Point2D) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}
	
	

}
