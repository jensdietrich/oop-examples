package lsp.compiler3;

public class B extends A {
	/**
	 * This compiles - the overriding method weakens the precondition:
	 * the caller does not have to be in the same package.
	 */
	@Override
	public void foo()  {
	}

}
