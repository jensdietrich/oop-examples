package lsp.compiler1;

import java.io.FileOutputStream;

public class B extends A {
	/**
	 * This compiles - the overriding method strengthens the postcondition (by returning instances of a subtype of OutputStream).
	 * Try to see what happens if the postconditions are weakened: replace FileOutputStream by Object.
	 */
	@Override
	public FileOutputStream getStream() {
		return null;
	}
}
