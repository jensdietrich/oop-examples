package lsp;

public class EvenNumberChecker {

	public boolean isEvenNumber(int i) {
		return i % 2 == 0;
	}

}
