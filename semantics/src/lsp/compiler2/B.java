package lsp.compiler2;

import java.io.IOException;
public class B extends A {
	/**
	 * This compiles - the overriding method strengthens the postcondition (by throwing a more special exception than Exception specified in the overridden method).
	 * Try to see what happens if the postconditions are weakened: replace IOException by Throwable.
	 */
	@Override
	public void processStream() throws IOException {
	}

}
