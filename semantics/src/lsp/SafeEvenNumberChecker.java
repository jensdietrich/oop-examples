package lsp;

public class SafeEvenNumberChecker extends EvenNumberChecker {
	// unsafe - violates LSP
	// stronger preconditions: expects non-negative number
	// weaker post-condition: does not guarantee that no runtime exception is thrown 
	@Override
	public boolean isEvenNumber(int i) {
		if (i < 0)
			throw new IllegalArgumentException(
					"Negative numbers are not allowed here");
		else
			return i % 2 == 0;
	}
}
