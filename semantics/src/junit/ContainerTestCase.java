package junit;

import static org.junit.Assert.*;

import org.junit.Test;



/**
 * A set of generic test cases for containers.
 * The container tested is the value of the container instance variable.
 * The value of this instance variable is null. Subclasses should set this value
 * using the setUp() method which is called (by the test application embedded in BlueJ)
 * before a test method is executed. 
 * @see SimpleContainerTestCase
 * @author  Jens Dietrich
 * @version 3.0 (now uses junit4)
 */
public abstract class ContainerTestCase
{
    protected Container container = null;
    /**
     * Default constructor for test class ContainerTestCase
     */
    public ContainerTestCase()
    {
    }


    /**
     * Test the add and the getSize() methods.
     * TEST DESCRIPTION: If we add one object, the size of the container should be 1.
     */
    @Test
    public void test1() {
        Object obj = "object1";
        container.add(obj);
        assertTrue(container.getSize()==1);
    }
    /**
     * Test the add and the getSize() methods.
     * TEST DESCRIPTION: If we add two object, the size of the container should be 2.
     */
    @Test
    public void test2() {
        Object obj1 = "object1";
        Object obj2 = "object2";
        container.add(obj1);
        container.add(obj2);
        assertTrue(container.getSize()==2);
    }
    /**
     * Test the add and the getObjects() methods.
     * TEST DESCRIPTION: If we add one object, the object is the first object in the array returned by getObjects().
     */
    @Test
    public void test3() {
        Object obj = "object";
        container.add(obj);
        assertTrue(container.getObjects()[0]==obj);
    }
    /**
     * Test the add and the getObjects() methods.
     * TEST DESCRIPTION: If we add two objects, the objects are the first / second object in the array returned by getObjects().
     */
    @Test
    public void test4() {
        Object obj1 = "object1";
        Object obj2 = "object2";
        container.add(obj1);
        container.add(obj2);
        assertTrue(container.getObjects()[0]==obj1);
        assertTrue(container.getObjects()[1]==obj2);
    }
    /**
     * Test the remove method.
     * TEST DESCRIPTION: If we add one object, we can remove it.
     */
    @Test
    public void test5() {
        Object obj = "object";
        container.add(obj);
        assertTrue(container.remove(obj));
    }
   /**
     * Test the remove method.
     * TEST DESCRIPTION: If we add two objects, we can remove the first object.
     */
    @Test
    public void test6() {
        Object obj1 = "object1";
        Object obj2 = "object2";
        container.add(obj1);
        container.add(obj2);
        assertTrue(container.remove(obj1));
    }
    /**
     * Test the remove method.
     * TEST DESCRIPTION: If we do not add an object, we cannot remove it.
     */
    @Test
    public void test7() {
        Object obj = "object";
        assertTrue(!container.remove(obj));
    }
    /**
     * This is a little stress test. We try to add 10000 objects.
     * TEST DESCRIPTION: If we add 10000 objects, the size of the container should be 10000.
     */
    @Test
    public void test8() {
        for (int i=0;i<10000;i++) container.add("object"+i);
        assertEquals(10000,container.getSize());
    }
}
