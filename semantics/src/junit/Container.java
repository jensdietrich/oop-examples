package junit;


/**
 * Interface for a container object - an object that manages other objects.
 * It is similar to an array, the main difference is that we can add an arbitrary number
 * of objects.
 * @author Jens Dietrich
 * @version 2.0
 */

public interface Container
{
	/**
	 * Add an object to the container.
	 * @param  obj an object
	 */
	public void add(Object obj);
	/**
	 * Remove an object from the container.
	 * @param obj an object
	 * @return true if the object has been removed, false otherwise
	 * false is in particular returned if the object has not been found
	 */
	public boolean remove(Object obj);
	/**
	 * Get the size of the container.
	 * @return the number of objects in the container
	 */
	public int getSize();
	/**
	 * Return the objects in the container as array.
	 * @return an array of objects
	 */
	public Object[] getObjects();
}
