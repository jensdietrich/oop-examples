package junit;

import java.util.*;
/**
 * Simple container implementation that uses a "real" container object
 * from the standard java library. Please do not care about the implementation !
 * What is important is that the interface is implemented and the test cases run!!
 * @author Jens Dietrich
 * @version 2.0
 */

public class SimpleContainer implements Container
{
    private ArrayList delegate = new ArrayList();
    /**
     * Add an object to the container.
     * @param  obj an object
     */
    public void add(Object obj) {
        delegate.add(obj);    
    }
    /**
     * Remove an object from the container.
     * @param obj an object
     * @return true if the object has been removed, false otherwise
     * false is in particular returned if the object has not been found
     */
    public boolean remove(Object obj) {
        return delegate.remove(obj);    
    }
    /**
     * Get the size of the container.
     * @return the number of objects in the container
     */
    public int getSize() {
        return delegate.size();    
    }
    /**
     * Return the objects in the container as array.
     * @return an array of objects
     */
    public Object[] getObjects() {
        return delegate.toArray();    
    }
}
