package junit;

import org.junit.Before;



/**
 * Test cases for SimpleContainer.
 * @see SimpleContainer
 * @author  Jens Dietrich
 * @version 3.0 (now uses junit4)
 */
public class SimpleContainerTestCase extends ContainerTestCase
{

    /**
     * Sets up the test fixture.
     * Called before every test case method.
     */
	@Before
    public void setUp()
    {
        container = new SimpleContainer();
    }

}
