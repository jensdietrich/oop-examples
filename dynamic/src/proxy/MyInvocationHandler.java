package proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class MyInvocationHandler implements InvocationHandler {

	@Override
	public Object invoke(Object obj, Method m, Object[] arg) throws Throwable {
		
		if (m.getName()=="toString" && arg==null) return " a proxy";
		System.out.println("Method " + m.getName() + " called on " + obj);
		return null;
	}



}
