package proxy;

import java.lang.reflect.Proxy;

public class RunExample {

	public static void main(String[] args) {
		
		MyInterface proxy = (MyInterface) Proxy.newProxyInstance(MyInterface.class.getClassLoader(),new Class[] { MyInterface.class },new MyInvocationHandler());
		proxy.foo();
	}

}
