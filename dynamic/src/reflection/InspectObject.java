package reflection;

import java.beans.*;



/**
 * Example how to analyse the properties using the java beans frameworks.
 * @author Jens Dietrich
 * @version 1.0
 */

public class InspectObject {

	/**
	 * Executable main method.
	 * @param runetime parameters
	 */
	public static void main(String[] args) throws Exception {
		Student s = new Student();
		inspect(s);
	}
	/**
	 * Inspect an object.
	 * @param obj an object
	 */
	public static void inspect(Object obj) throws Exception {
		BeanInfo beanInfo = Introspector.getBeanInfo(obj.getClass());
		PropertyDescriptor[] properties = beanInfo.getPropertyDescriptors();
		// attention: new JDK5 syntax used here
		System.out.println("Inspecting object " + obj);
		for (PropertyDescriptor property:properties) {
			System.out.print(property.getName());
			Object value = property.getReadMethod().invoke(obj,new Object[]{});
			System.out.print(" = ");
			System.out.println(value);
		}
		
	}

}
