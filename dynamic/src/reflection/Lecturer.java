package reflection;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.Date;

/**
 * Example domain model class.
 * @author Jens Dietrich
 * @version 1.0
 */
public class Lecturer {
	// properties, setters and getters are tool generated
	private int id = 42;
	private String name = "Bush";
	private String firstName = "George";
	private java.util.Date dob = new java.util.Date();
	
	private PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);
	
	/**
	 * @return Returns the dob.
	 */
	public java.util.Date getDob() {
		return dob;
	}
	/**
	 * Set the new date of birth.
	 * @param dob The dob to set.
	 */
	public void setDob(java.util.Date dob) {
		Date old = this.dob;
		this.dob = dob;
		this.propertyChangeSupport.firePropertyChange("dob",old,dob);
	}
	/**
	 * @return Returns the firstName.
	 */
	@DBMapping(column="first_name")
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName The firstName to set.
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return Returns the id.
	 */
	@DBMapping(column="ID")
	public int getId() {
		return id;
	}
	/**
	 * @param id The id to set.
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return Returns the name.
	 */
	@DBMapping(column="name")
	public String getName() {
		return name;
	}
	/**
	 * @param name The name to set.
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * Add a property change listener.
	 * @param l a listener
	 */
	public void addPropertyChangeListener(PropertyChangeListener l) {
		this.propertyChangeSupport.addPropertyChangeListener(l);
	}
	/**
	 * Remove a property change listener.
	 * @param l a listener
	 */
	public void removePropertyChangeListener(PropertyChangeListener l) {
		this.propertyChangeSupport.removePropertyChangeListener(l);
	}

}
