package reflection;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Date;

public class BoundPropertyExample {
	public static void main (String[] args) {
		Lecturer lecturer = new Lecturer();
		// create a new listener
		PropertyChangeListener listener = new PropertyChangeListener() {
			public void propertyChange(PropertyChangeEvent e) {
				System.out.print("Property ");
				System.out.print(e.getPropertyName());
				System.out.print(" has changed, the new value is ");
				System.out.println(e.getNewValue());
			}
		};
		lecturer.addPropertyChangeListener(listener);
		// this will now trigger an event
		lecturer.setDob(new Date());		
	}
}
