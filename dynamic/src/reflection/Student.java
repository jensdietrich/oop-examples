package reflection;
/**
 * Example domain model class.
 * @author Jens Dietrich
 * @version 1.0
 */
public class Student {
	// properties, setters and getters are tool generated
	private int id = 42;
	private String name = "Bush";
	private String firstName = "George";
	private java.util.Date dob = new java.util.Date();
	/**
	 * @return Returns the dob.
	 */
	public java.util.Date getDob() {
		return dob;
	}
	/**
	 * @param dob The dob to set.
	 */
	public void setDob(java.util.Date dob) {
		this.dob = dob;
	}
	/**
	 * @return Returns the firstName.
	 */
	@DBMapping(column="first_name")
	public String getFirstName() {
		return firstName;
	}
	/**
	 * @param firstName The firstName to set.
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	/**
	 * @return Returns the id.
	 */
	@DBMapping(column="ID")
	public int getId() {
		return id;
	}
	/**
	 * @param id The id to set.
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return Returns the name.
	 */
	@DBMapping(column="name")
	public String getName() {
		return name;
	}
	/**
	 * @param name The name to set.
	 */
	public void setName(String name) {
		this.name = name;
	}

}
