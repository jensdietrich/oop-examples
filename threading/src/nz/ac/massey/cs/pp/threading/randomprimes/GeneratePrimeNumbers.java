package nz.ac.massey.cs.pp.threading.randomprimes;

import java.util.ArrayList;
import java.util.List;

/**
 * Simple program that computes some large prime numbers without using threads.
 * @author jens dietrich
 */
public class GeneratePrimeNumbers {

	public static void main(String[] args) {
		long timeBefore = System.currentTimeMillis();
		List<Integer> primeNumbers = new ArrayList<Integer>();
		for (int i=0;i<Settings.COUNT;i++) {
			int nextPrimeNumber = RandomPrimeNumberGenerator.getNextRandomPrime(Settings.MAX_NUMBER);
		}
		long timeAfter = System.currentTimeMillis();
		System.out.println("Generated " + Settings.COUNT + " prime numbers, this took " + (timeAfter-timeBefore) + "ms");
	}

}
