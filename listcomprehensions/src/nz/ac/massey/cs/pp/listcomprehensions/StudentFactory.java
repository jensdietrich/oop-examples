package nz.ac.massey.cs.pp.listcomprehensions;

import java.util.ArrayList;
import java.util.List;

/**
 * Utility to create some student instances.
 * @author jens dietrich
 */
public class StudentFactory {
	public static List<Student> getStudents() {
		List<Student> students = new ArrayList<Student>();
		students.add(new Student(1,"Tom","Taylor","Software Engineering"));
		students.add(new Student(2,"Jim","Smith","Computer Science"));
		students.add(new Student(3,"Tim","Brown","Software Engineering"));
		students.add(new Student(4,"Alex","Johnson","Computer Science"));
		students.add(new Student(5,"Graham","Jenson","Information Technology"));
		students.add(new Student(6,"Jevon","Wright","Software Engineering"));
		students.add(new Student(7,"Kate","Mahone","Software Engineering"));
		students.add(new Student(8,"Catherine","McDonald","Software Engineering"));
		students.add(new Student(9,"Tom","Brown","Software Engineering"));
		return students;
	}
}
