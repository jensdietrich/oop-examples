package nz.ac.massey.cs.pp.listcomprehensions.guava;

import nz.ac.massey.cs.pp.listcomprehensions.Student;
import com.google.common.base.Function;

/**
 * Function to turn students into full names. 
 * @author jens dietrich
 */

public class GetFullName implements Function<Student, String> {

	@Override
	public String apply(Student student) {
		return student.getFirstName() + " " + student.getName();
	}

}
