package nz.ac.massey.cs.pp.listcomprehensions.guava;

import nz.ac.massey.cs.pp.listcomprehensions.Student;
import com.google.common.base.Predicate;
/**
 * Simple filter for Student instances based on the firstName property. 
 * @author jens dietrich
 */
public class FilterByFirstName implements Predicate<Student> {
	private String firstName = null; 
	public FilterByFirstName(String firstName) {
		super();
		this.firstName = firstName;
	}
	@Override
	public boolean apply(Student student) {
		return student.getFirstName().equals(firstName);
	}

}
