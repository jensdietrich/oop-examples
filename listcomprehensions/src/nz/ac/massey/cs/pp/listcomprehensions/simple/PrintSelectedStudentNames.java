package nz.ac.massey.cs.pp.listcomprehensions.simple;

import java.util.ArrayList;
import java.util.Collection;
import nz.ac.massey.cs.pp.listcomprehensions.Student;
import nz.ac.massey.cs.pp.listcomprehensions.StudentFactory;

/**
 * This is "classical" procedural Java code used to process collections.
 * @author jens dietrich
 */

public class PrintSelectedStudentNames {

	public static void main(String[] args) {
		Collection<Student> students = StudentFactory.getStudents();
		String major = "Software Engineering";
		String firstName = "Tom";
		
		Collection<String> names = new ArrayList<>();
		for (Student student:students) {
			if (student.getMajor().equals(major) && student.getFirstName().equals(firstName)) {
				names.add(student.getFirstName() + " " + student.getName());
			}
		}
		
		for (String name:names) {
			System.out.println(name);
		}
	}

}
