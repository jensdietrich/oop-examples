# example how to integrate jruby and swing
 
require 'java'
frame = javax.swing.JFrame.new("JRuby panel")
frame.size = java.awt.Dimension.new(100,100)
frame.location = java.awt.Point.new(44,44)
button = javax.swing.JButton.new("ok")

 class Button_Clicked 
   include java.awt.event.ActionListener
    def initialize(view)
       @view = view
    end
    def actionPerformed(event)
      @view.dispose
    end
 end
 button.addActionListener Button_Clicked.new(frame)
    
frame.add(button)
frame.show
