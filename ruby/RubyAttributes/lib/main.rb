# shows how meta programming is used to generate getter and setters
 

require 'person'
# create an object and play around with it
p = Person.new 42
p.firstName="John"
p.name="Smith"
puts p.fullName
