# block without parameters 
def callBlock
  yield
end
callBlock {puts "Hi"}

# block with parameters 
def math(k,l)
  puts "before"
  yield k,l
  puts "after"
end
math(3,4) {|i,j| puts i+j}
math(3,4) {|i,j| puts i*j}
