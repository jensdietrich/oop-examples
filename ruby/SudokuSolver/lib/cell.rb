# To change this template, choose Tools | Templates
# and open the template in the editor.

class Cell
  attr_accessor :column,:row,:value,:properties
  def initialize(col,row,val)
    @column=col
    @row=row
    @value=val
    @properties = Hash.new
  end
  def clone
    Cell.new(@column,@row,@value)
  end
end
