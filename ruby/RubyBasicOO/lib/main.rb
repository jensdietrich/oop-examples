# standard OO principles in ruby: inheritance, overriding, variable access
require 'student'
# create an object and play around with it
p = Person.new("Brown","Tom")
puts p.fullName
# change type
p = Student.new(42,"Smith","John")
puts p.fullName
# understands sid not defined in Person
p.sid=43
# dynamic method lookup!
puts p.fullName