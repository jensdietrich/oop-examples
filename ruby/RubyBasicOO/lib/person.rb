# 
# To change this template, choose Tools | Templates
# and open the template in the editor.
 

class Person
  protected
  def initialize(name,firstName)
    @name = name
    @firstName = firstName
  end
  public
  #getters
  def name
    return @name
  end
  def firstName
    return @firstName
  end
  def fullName 
    return self.firstName + " " + self.name
  end
  #setters
  def name=(n)
    @name = n
  end
end
