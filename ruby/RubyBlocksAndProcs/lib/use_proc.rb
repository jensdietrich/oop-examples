# To change this template, choose Tools | Templates
# and open the template in the editor.



# add our own method to array:
class Array
  def iterate!(code)
    self.each_with_index do |n, i|
      self[i] = code.call(n)
    end
  end
end

double = Proc.new do |each|
  each * 2
end

# reuse procedure - this is not possible with blocks1
puts [1,2,3,4].iterate!(double).inspect
puts [5,6,7].iterate!(double).inspect

