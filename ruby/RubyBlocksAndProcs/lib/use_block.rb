
array = [1,2,3,4]
# note that the bang version modyfies the array
array.collect! {|each| each*2}
puts array.inspect

puts {|each| each*2}.inspect
