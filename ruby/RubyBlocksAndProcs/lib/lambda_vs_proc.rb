# from http://www.robertsosinski.com/2008/12/21/understanding-ruby-blocks-procs-and-lambdas/

# proc returns when finished
def proc_return
  Proc.new { return "Proc.new"}.call
  return "proc_return method finished"
end

# lambda returns control to method
def lambda_return
  lambda { return "lambda" }.call
  return "lambda_return method finished"
end

puts proc_return
puts lambda_return

puts lambda {return "lambda" }.class
puts Proc.new { return "Proc.new"}.class