# example from "Beyond Java, p143"
# tested with Ruby 1.8.7 on Mac, does not work with 1.9 or JRuby 1.7


puts "start !"

def loop 
  for i in 1..5 do
    puts i
    callcc {|continuation| return continuation} if i==2
    puts ".."
  end
  return nil
end
cont = loop();
puts "part1 done"
cont.call if (cont!=nil)
puts "part2 done"

