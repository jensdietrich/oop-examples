# example from Flanagan, Matsumoto: The Ruby Programming Language

require 'fibre'
def fg(x0,y0)
    Fiber.new do
       x,y=x0,y0
       loop do
         Fiber yield y
         x,y=y,x+y
       end
    end
end

g = fg(0,1)
10.times { print g.resume, " " }
