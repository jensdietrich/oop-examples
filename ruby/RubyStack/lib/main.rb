def doSomething 
  if /.badFunction./ =~ caller(0).to_s
    puts "did nothing - I am only working for good clients"
  else
    puts "did something"
  end
end

def goodFunction
  doSomething
end

def badFunction
  doSomething
end

goodFunction

badFunction
