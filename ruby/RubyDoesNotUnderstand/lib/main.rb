# Smalltalk like doesNotUnderstand - handy to implement proxies
# uses reflection as well in proxy
require 'proxy'
require 'target'
dummy = Proxy.new(Target.new)
dummy.doIt
dummy.test
