# 
# To change this template, choose Tools | Templates
# and open the template in the editor.
 

class Proxy
  def initialize(t)
    @target = t
  end
  public
  def test
    puts "test"
  end
  def method_missing(m)
    puts "this did not work very well, lets try to send it real object "+@target.to_s
    @target.send(m)
  end
end
