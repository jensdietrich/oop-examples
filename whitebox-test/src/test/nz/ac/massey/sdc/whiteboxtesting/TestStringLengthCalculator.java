package test.nz.ac.massey.sdc.whiteboxtesting;

import org.junit.Test;
import nz.ac.massey.cs.webtech.whiteboxtesting.StringLengthCalculatorServlet;

import com.mockobjects.servlet.*;
/**
 * Test case(s) using mock objects. A "fake" web server is used, and the doGet of the servlet method is called
 * directly. Tests check for expected strings in the output page, and for the content type.
 * @author jens dietrich
 */
public class TestStringLengthCalculator extends junit.framework.TestCase{
    
    /** Creates a new instance of TestCase1 */
    public TestStringLengthCalculator() {
    }
    // test whether the correct web page is produced
    @Test
    public void testResultContent() throws Exception {
        // 1. prepare request
        MockHttpServletRequest req = new MockHttpServletRequest();
        MockHttpServletResponse res = new MockHttpServletResponse();
        req.setupAddParameter("string","this is a magic string");
        
        // 2. invoke servlet
        StringLengthCalculatorServlet  servlet = new StringLengthCalculatorServlet();
        servlet.doGet(req,res);
        
        // 3. verify
        String webPageAsString = res.getOutputStreamContents();
        assertTrue(webPageAsString.contains("The length of the string is 22"));
    }
    // test whether the content type is text/html
    @Test
    public void testResultContentType() throws Exception {
        // 1. prepare request
        MockHttpServletRequest req = new MockHttpServletRequest();
        MockHttpServletResponse res = new MockHttpServletResponse();
        req.setupAddParameter("string","this is a magic string");
        res.setExpectedContentType("text/html");
        
        // 2. invoke servlet
        StringLengthCalculatorServlet  servlet = new StringLengthCalculatorServlet();
        servlet.doGet(req,res);
        
        // 3. verify
        res.verify();       
    }
    
}
