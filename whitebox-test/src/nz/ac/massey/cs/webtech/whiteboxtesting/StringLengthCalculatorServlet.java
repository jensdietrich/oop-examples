
package nz.ac.massey.cs.webtech.whiteboxtesting;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 * Very simple example used to show how testing works.
 * A string is passed as a query parameter (http://server.com?string=test),
 * and the web page prints the length of the string. 
 * @author jens dietrich
 */
public class StringLengthCalculatorServlet extends HttpServlet {

    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        String string = request.getParameter("string");
        out.println("<html>");
        out.println("<head>");
        out.println("<title>StringLength Calculator</title>");
        out.println("</head>");
        out.println("<body>");
        out.println("<h1>StringLength Calculator</h1>");
        out.println("<form>");
        out.print("<input type=\"text\" name=\"string\" value=\"");
        out.print(string==null?"":string);
        out.println("\">");
        out.println("<input type=\"submit\">");
        out.println("</form>");
        out.print("The length of the string is ");
        out.println(string==null?0:string.length());
        out.println("</body>");
        out.println("</html>");

        out.close();
    }


    public void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }


    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    public String getServletInfo() {
        return "Short description";
    }
    
}
