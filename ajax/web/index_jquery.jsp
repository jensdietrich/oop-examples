<%@page contentType="text/html"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="jquery-1.7.2.js"></script>
        <script>
            function check4matchingNames(key) {
                $('#matchingnames').load('validate.jsp?name='+key);
            } 
        </script>    
    </head>
    <body>

    <h1>The AJAX First Name DB - JQuery Version</h1>
    
    This example uses the JQuery API for the AJAX call.
        
    <p/>
    <form name="form1">
        Enter a name here:<br/>
        <input type="text" name="name" id="name" value="" size="50" onkeyup="check4matchingNames(this.value)"/>
        <p/>
        <textarea name="matchingnames" id="matchingnames" cols="40" rows="5" readonly></textarea>
    </form>
    
    
    <p/><hr/><p/>
    <a href="index.jsp">version not using jquery</a>
    
    </body>
</html>
