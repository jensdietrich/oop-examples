package nz.ac.massey.cs.pp.swing101.simple;

import javax.swing.*;

public class PopupBetterWindow {

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setTitle("Hello World");
		frame.setLocation(100, 100);
		frame.setSize(300,300);
		frame.setVisible(true);
	}

}
