package nz.ac.massey.cs.pp.swing101.simple;

import javax.swing.*;

public class PopupBetterWindowWithButtons {

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setTitle("Hello World");
		frame.setLocation(100, 100);
		frame.setSize(300,300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(new JButton("a button"));		
		frame.setVisible(true);
	}

}
