package nz.ac.massey.cs.pp.swing101.layouts;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class NestedPanelsWithBorder {

	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.setLocation(100, 100);
		frame.setSize(500,300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		frame.getContentPane().setLayout(new BorderLayout());
		// toolbar
		JPanel toolbar = new JPanel();
		toolbar.setLayout(new FlowLayout(FlowLayout.LEFT));
		toolbar.add(new JButton("exit"));
		toolbar.add(new JButton("load"));
		toolbar.add(new JButton("save"));
		frame.getContentPane().add(toolbar,BorderLayout.NORTH);
		
		JTextArea textArea = new JTextArea("edit me");
		textArea.setBorder(BorderFactory.createEmptyBorder(5,10,5,10));
		frame.getContentPane().add(textArea,BorderLayout.CENTER);
		
		frame.getContentPane().add(new JLabel("info"),BorderLayout.SOUTH);
	
		frame.setVisible(true);
	}

}
