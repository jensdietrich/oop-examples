package nz.ac.massey.cs.pp.swing101.events;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
/**
 * The frame class directly implements the event listener interface.
 */
public class MyFrame2 extends JFrame  implements ActionListener {
	
	private JButton exitButton = new JButton("exit");
	private JButton saveButton = new JButton("save");
	private JButton loadButton = new JButton("load");
	
	public MyFrame2() {
		super();
		init();
	}
	
	private void init() {
		this.getContentPane().setLayout(new FlowLayout());
		this.getContentPane().add(exitButton);
		this.getContentPane().add(loadButton);
		this.getContentPane().add(saveButton);
		this.setLocation(100,100);
		this.setSize(300,200);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.exitButton.addActionListener(this);
	}

	public static void main(String[] args) {
		MyFrame2 frame = new MyFrame2();
		frame.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource()==this.exitButton) {
			this.dispose();
		}
	}


}
