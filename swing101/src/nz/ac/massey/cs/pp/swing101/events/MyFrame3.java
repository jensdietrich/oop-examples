package nz.ac.massey.cs.pp.swing101.events;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
/**
 * Anonymous inner classes are used to implement event handlers.
 */
public class MyFrame3 extends JFrame {
	
	private JButton exitButton = new JButton("exit");
	private JButton saveButton = new JButton("save");
	private JButton loadButton = new JButton("load");
	
	public MyFrame3() {
		super();
		init();
	}
	
	private void init() {
		this.getContentPane().setLayout(new FlowLayout());
		this.getContentPane().add(exitButton);
		this.getContentPane().add(loadButton);
		this.getContentPane().add(saveButton);
		this.setLocation(100,100);
		this.setSize(300,200);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.exitButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				dispose();
			}
		});
	}

	public static void main(String[] args) {
		MyFrame3 frame = new MyFrame3();
		frame.setVisible(true);
	}




}
