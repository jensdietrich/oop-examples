package nz.ac.massey.cs.asdc.patterns.visitor.iteration3;

import static org.junit.Assert.*;

import org.junit.Test;

public class Test1 {

	@Test
	public void test() {
		// create object representing expression y = x+(z-42)
		Condition condition = new Condition(
			Operator.EQ,
			new Variable("y"),
			new ComplexTerm(
					Function.PLUS,
					new Variable("x"),
					new ComplexTerm(
							Function.MINUS,
							new Variable("z"),
							new Constant(42)
					)
			)
		);
		
		VariableFinder visitor = new VariableFinder();
		condition.accept(visitor);
		
		assertEquals(3,visitor.getVariables().size());
		assertTrue(visitor.getVariables().contains("x"));
		assertTrue(visitor.getVariables().contains("y"));
		assertTrue(visitor.getVariables().contains("z"));
	}

}
