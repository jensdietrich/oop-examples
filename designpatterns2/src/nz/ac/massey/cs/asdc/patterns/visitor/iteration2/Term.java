package nz.ac.massey.cs.asdc.patterns.visitor.iteration2;

/**
 * Abstract class to represent arithmetics terms, see subclasses for details.
 * @author jens dietrich
 */
public abstract class Term implements Expression {
	public boolean isComplex() {
		return false;
	}
}
