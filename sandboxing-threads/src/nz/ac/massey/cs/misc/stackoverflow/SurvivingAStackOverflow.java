package nz.ac.massey.cs.misc.stackoverflow;

import nz.ac.massey.cs.misc.threats.RecursionWithoutTermination;


public class SurvivingAStackOverflow {
	
	public static void main(String[] args) throws Exception {
		// some time to connect VisualVM to monitor this
		Thread.sleep(10000);
		
		Runnable sandboxed = new Runnable() {
				@Override public void run() {
					try {
						new RecursionWithoutTermination().run();
					}
					catch (StackOverflowError x) {
						System.err.println("Thread crashed with stack overflow");
					} 
				}
			};
			
		Thread thread = new Thread(sandboxed,"infinite loop");
		thread.start();
		thread.join();
		System.out.println(thread.getState());
		while (true) {
				Thread.sleep(1000);
				System.out.println("I am still alive");
		}

	}
}
