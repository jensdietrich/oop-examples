package nz.ac.massey.cs.misc.securitymanager;

import static org.junit.Assert.*;
import nz.ac.massey.cs.misc.threats.FileSystemAccess;

import org.junit.BeforeClass;
import org.junit.Test;

public class Tests {
	
	private static MySecurityManager securityManager = new MySecurityManager();
	
	@BeforeClass
	public static void setUp() throws Exception {
		System.setSecurityManager(securityManager); 
	}

	
	@Test
	public void testWithSecurityInRestrictedThread () throws InterruptedException  {
		
			FileSystemAccess writer = new FileSystemAccess();
			Thread thread = new Thread(writer,"testWithSecurityInRestrictedThread");
			
			securityManager.restrict(thread); // restrict the thread running the code accessing the file
			
			thread.start();
			thread.join();
			
			assertFalse(writer.isSuccess());
	}
	
	@Test
	public void testWithSecurityInUnrestrictedThread1 () throws InterruptedException  {
		
			FileSystemAccess writer = new FileSystemAccess();
			Thread thread = new Thread(writer,"testWithSecurityInUnrestrictedThread");
			thread.start();
			thread.join();
			
			assertTrue(writer.isSuccess());
	}

	@Test
	public void testWithSecurityInUnrestrictedThread2 () throws InterruptedException  {
		
			FileSystemAccess writer = new FileSystemAccess();
			Thread thread = new Thread(writer,"testWithSecurityInUnrestrictedThread");
			
			securityManager.restrict(thread); 
			securityManager.unrestrict(thread); 
			
			thread.start();
			thread.join();
			
			assertTrue(writer.isSuccess());
	}

}
