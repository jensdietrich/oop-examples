package nz.ac.massey.sdc.coverage;

public class Person {
	private String firstName = null;
	private String name = null;
	private Gender gender = Gender.MALE;
	private Address address = null;
	private boolean isMarried = false;
	private int age = 18;

	public Person(String firstName, String name, Gender gender,
			Address address, boolean isMarried, int age) {
		super();
		this.firstName = firstName;
		this.name = name;
		this.gender = gender;
		this.address = address;
		this.isMarried = isMarried;
		this.age = age;
	}

	public Person() {
		super();
	}

	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Gender getGender() {
		return gender;
	}
	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}
	

	public boolean isMarried() {
		return isMarried;
	}

	public void setMarried(boolean isMarried) {
		this.isMarried = isMarried;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	
	public String getFullName () {
		String namePart = this.getFirstName() + " " + this.getName(); 
		String salutationPart = null;
		if (gender==Gender.MALE) {
			salutationPart = "Mr.";
		}
		else if (gender==Gender.FEMALE) {
			if (this.isMarried) {
				salutationPart = "Mrs.";
			}
			else if (age<19){
				salutationPart = "Miss";
			}
			else {
				salutationPart = "Ms.";
			}
		}
		return salutationPart + " " + namePart;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + age;
		result = prime * result
				+ ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((gender == null) ? 0 : gender.hashCode());
		result = prime * result + (isMarried ? 1231 : 1237);
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}




	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (age != other.age)
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (gender != other.gender)
			return false;
		if (isMarried != other.isMarried)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
}
