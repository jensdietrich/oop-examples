
package nz.ac.massey.webtech.sseperformance;

import java.io.PrintWriter;

/**
 * Utility to generate event data.
 * @author jens dietrich
 */
public class EventGenerator {
    
    static void nextEvent (PrintWriter out) {
        out.print("event: server-time\n\n");
        out.print("retry: 1000\n\n");
        out.print("data: foo\n\n");
    }
    
}
