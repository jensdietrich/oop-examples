/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package nz.ac.massey.webtech.sseperformance;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author jens dietrich
 */
public class RequestPerEventBuffered extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/event-stream");
        response.setCharacterEncoding ("UTF-8");
        PrintWriter out = response.getWriter();
        
        // here we use an event counter to control connections
        long start = System.currentTimeMillis();
        long timeout = 1000;
        long wait = 200;
        
        while ((System.currentTimeMillis()-start)<timeout) {
            EventGenerator.nextEvent(out);
            out.flush();
            System.out.println("flushing stream");
            try {
                Thread.sleep(wait);
            } catch (InterruptedException ex) {
                System.out.println(ex.getMessage());
            }
        }
        
        System.out.println("closing stream");
        out.close();
    
    }
}
