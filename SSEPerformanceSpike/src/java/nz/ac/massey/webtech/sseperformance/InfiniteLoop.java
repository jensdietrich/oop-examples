
package nz.ac.massey.webtech.sseperformance;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author jens dietrich
 */
public class InfiniteLoop extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/event-stream");
        response.setCharacterEncoding ("UTF-8");
        PrintWriter out = response.getWriter();
        this.getServletContext().log("entering while(true) loop");
        System.out.println("entering while(true) loop");
        while (true) {
            EventGenerator.nextEvent(out);
            
            try {
                out.flush(); 
                Thread.sleep(1000);
            } catch (Exception ex) {
                // Logger.getLogger(InfiniteLoop.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println("while(true) in doGet exited with exception");
            }
        }
       
    }

}
