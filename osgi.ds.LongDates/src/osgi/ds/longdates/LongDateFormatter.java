package osgi.ds.longdates;

import java.text.DateFormat;
import java.util.Date;

import osgi.ds.servicedefs.DateFormatter;


public class LongDateFormatter implements DateFormatter {

	@Override
	public String toString(Date d) {
		return DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG).format(d);
	}

}
