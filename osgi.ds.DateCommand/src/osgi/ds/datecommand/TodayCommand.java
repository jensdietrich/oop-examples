package osgi.ds.datecommand;

import java.util.Date;

import org.eclipse.osgi.framework.console.CommandInterpreter;
import org.eclipse.osgi.framework.console.CommandProvider;

import osgi.ds.servicedefs.DateFormatter;


/**
 * Simple component.
 * Provides a command (this means that the respective equinox bundle must be present).
 * Consumes a data formatter.
 * @author Jens Dietrich 
 */

public class TodayCommand implements CommandProvider {
	
	private DateFormatter formatter = null;
	
	public void _today(CommandInterpreter ci) {
		Date timestamp = new Date();
		ci.print("the current date and time is: " + (formatter==null?(timestamp.toString() + " (no formatter available)"):(formatter.toString(timestamp))));
	}

	@Override
	public String getHelp() {
		return "today command";
	}
	
	public void setDateFormatter(DateFormatter formatter) {
		System.out.println("Setting formatter to " + formatter);
		this.formatter = formatter;
	}
	
	public void unsetDateFormatter(DateFormatter f) {
		// careful - unset is called after set if a service is removed and an alternative is available
		// therefore we must check this.formatter
		
		if (this.formatter==f) {
			this.formatter = null;
			System.out.println("Unsetting formatter " + f);
		}
	}
	
	// some simple lifecycle methods
	public void activate() {
		System.out.println("Activating component " + this);
	}
	public void deactivate() {
		System.out.println("Deactivating component " + this);
	}



}
