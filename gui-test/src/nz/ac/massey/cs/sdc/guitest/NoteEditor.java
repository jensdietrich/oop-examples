package nz.ac.massey.cs.sdc.guitest;

import java.awt.EventQueue;
import javax.swing.JInternalFrame;
import java.awt.Color;
import javax.swing.JTextPane;
import java.awt.BorderLayout;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

/**
 * Editor component to manage a single yellow "sticky" note within the note manager.
 * This class was designed using the Eclipse ui builder. 
 * @author jens dietrich
 */
public class NoteEditor extends JInternalFrame {
	
	private Note model = null;
	private JTextField titleEditor;
	JTextPane textEditor;

	public void setModel(Note model) {
		this.model = model;
		this.titleEditor.setText(model.getTitle());
		this.textEditor.setText(model.getText());
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NoteEditor frame = new NoteEditor();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public NoteEditor() {
		getContentPane().setBackground(new Color(240, 230, 140));
		
		textEditor = new JTextPane();
		textEditor.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				model.setText(textEditor.getText());
			}
		});
		textEditor.setBackground(new Color(238, 232, 170));
		getContentPane().add(textEditor, BorderLayout.CENTER);
		
		titleEditor = new JTextField();
		titleEditor.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				model.setTitle(titleEditor.getText());
			}
		});
		titleEditor.setBackground(new Color(240, 230, 140));
		titleEditor.setFont(new Font("Lucida Grande", Font.BOLD, 15));
		titleEditor.setHorizontalAlignment(SwingConstants.LEFT);
		getContentPane().add(titleEditor, BorderLayout.NORTH);
		titleEditor.setColumns(10);
		setBounds(100, 100, 450, 300);

	}

	public Note getModel() {
		return model;
	}
	
	// expose ui components for testing
	
	public JTextField getTitleEditor() {
		return titleEditor;
	}

	public JTextPane getTextEditor() {
		return textEditor;
	}
	
	
}
