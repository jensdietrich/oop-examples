package nz.ac.massey.cs.sdc.guitest;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JDesktopPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
/**
 * User interface component to manage yellow "sticky" notes using a multi-document interface.
 * This class was designed using the Eclipse ui builder. 
 * @author jens dietrich
 */
public class NotesManager extends JFrame {
	private JPanel contentPane;
	private JDesktopPane desktopPane;
	private JButton btnExit;
	private JButton btnRemoveNote;
	private JButton btnAddNote;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NotesManager frame = new NotesManager();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public NotesManager() {
		setTitle("Simple Notes Manager");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 830, 542);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		desktopPane = new JDesktopPane();
		contentPane.add(desktopPane, BorderLayout.CENTER);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.SOUTH);
		
		btnAddNote = new JButton("add new note");
		btnAddNote.setName("btnAddNote");
		btnAddNote.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addNote();
			}
		});
		
		
		btnExit = new JButton("exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnExit.setName("btnExit");
		
		btnRemoveNote = new JButton("remove note");
		btnRemoveNote.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				removeNote();
			}
		});
		btnRemoveNote.setName("btnRemoveNote");
		
		panel.add(btnAddNote);
		panel.add(btnRemoveNote);
		panel.add(btnExit);
	}
	
	private void addNote() {
		NoteEditor selectedNoteEditor = (NoteEditor) desktopPane.getSelectedFrame();
		NoteEditor noteEditor = new NoteEditor();
		noteEditor.setModel(new Note());
		// add a little offset so that components don't overlap
		if (selectedNoteEditor!=null) {
			noteEditor.setLocation(selectedNoteEditor.getLocation().x+20,selectedNoteEditor.getLocation().y+20);
		}
		noteEditor.setVisible(true);
		desktopPane.add(noteEditor);
		try {
			noteEditor.setSelected(true);
	    } catch (java.beans.PropertyVetoException x) {}
	}
	
	private void removeNote() {
		NoteEditor selectedNoteEditor = (NoteEditor) desktopPane.getSelectedFrame();
		if (selectedNoteEditor!=null) {
			// desktopPane.remove(selectedNoteEditor);
			selectedNoteEditor.dispose();
		}
	}
		
	// add a couple of getters for testing
	// note that testing frameworks like abbot use another strategy: "matchers" to find components
	// within the component hierarchy 

	public JDesktopPane getDesktopPane() {
		return desktopPane;
	}

	public JButton getBtnExit() {
		return btnExit;
	}

	public JButton getBtnRemoveNote() {
		return btnRemoveNote;
	}

	public JButton getBtnAddNote() {
		return btnAddNote;
	}
	
	public NoteEditor[] getNoteEditors() {
		JInternalFrame[] frames = desktopPane.getAllFrames();
		NoteEditor[] noteEditors = new NoteEditor[frames.length];
		for (int i=0;i<noteEditors.length;i++) {
			noteEditors[i] = (NoteEditor)frames[i];
		}
		return noteEditors;
	}
}
