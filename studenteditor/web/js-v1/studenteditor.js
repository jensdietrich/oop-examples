
// single model
function Student(data) {
    // observable properties - they fire events when changed to notify UI
    this.name = ko.observable(data.name);
    this.firstName = ko.observable(data.firstName);
    this.id = ko.observable(data.id);
    // derived value
    this.fullName = ko.computed(function() {
        return this.firstName() + " " + this.name();
    }, this);

}

// collection model
function StudentListViewModel() {
    // Data
    var self = this;
    self.students = ko.observableArray([]);
    // factories for new values
    self.newStudentName = ko.observable();
    self.newStudentFirstName = ko.observable();
    
    self.students (
        [
            {name: "Taylor",firstName: "Tom"},
            {name: "Smith",firstName: "Sam"}
        ]
    );

    self.add = function() {
        self.students.push(new Student({ name: self.newStudentName(), firstName: self.newStudentFirstName() }));
        // reset 
        self.newStudentName("");
        self.newStudentFirstName("");
    };
    self.remove = function(student) {
        self.students.remove(student);
    };   

}
