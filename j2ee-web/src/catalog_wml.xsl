<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:template match="/">
	<wml>
            <card id="1" title="CD's">
                <p align="center">
		<xsl:for-each select="catalog/cd">
                <xsl:value-of select="title"/> : 
                <xsl:value-of select="artist"/><br/>
		</xsl:for-each>
                </p>
            </card>
        </wml>
</xsl:template>
</xsl:stylesheet>
