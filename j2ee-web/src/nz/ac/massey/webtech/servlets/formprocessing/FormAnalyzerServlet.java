package nz.ac.massey.webtech.servlets.formprocessing;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;

/**
 * Servlet used to process forms.
 * @author  Jens Dietrich   
 * @version 1.0
 */
public class FormAnalyzerServlet extends HttpServlet {
    
    /** Initializes the servlet.
     */
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        
    }
    
    /** Destroys the servlet.
     */
    public void destroy() {
        
    }
    
    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
       
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Form Analyzer Servlet</title>");
        out.println("</head>");
        out.println("<body>");
         
        out.println("<h1>Submitted Form Values</h1>");
        out.println("<table border><th>Parameter Name</th><th>Parameter Value</th>");
        // list headers send by the client
        for (Enumeration parameters = request.getParameterNames();parameters.hasMoreElements();) {
            String parameter = parameters.nextElement().toString();
            String value = request.getParameter(parameter);
            out.print("<tr><td>");
            out.print(parameter);
            out.print("</td><td>");
            out.print(value);
            out.println("</td></tr>");
        }
                
        out.println("</table>");
        // use headers
        out.print("This form has been submitted by <strong>");
        out.print(request.getHeader("Referer"));
        out.print("</strong>");
        
        out.println("</body>");
        out.println("</html>");
        
        out.close();
    }
    
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    
}
