package nz.ac.massey.webtech.servlets.xml;

/**
 * Implementation of the catalog servlet that produces HTML on the server.
 * @author  Jens Dietrich   
 * @version 1.0
 */
public class CatalogServlet4ServerGeneratedHtml extends AbstractCatalogServlet{
    
    /** Constructor */
    public CatalogServlet4ServerGeneratedHtml() {
        super();
    }
    // get the content type returned
    protected String getContentType() {
        return "text/html";
    }
    // get the stylesheet (url)
    protected String getStylesheetUrl() {
        return "/catalog_html.xsl";
    }
    
}
