package nz.ac.massey.webtech.servlets.xml;

/**
 * Implementation of the catalog servlet that produces WML.
 * @author  Jens Dietrich   
 * @version 1.0
 */
public class CatalogServlet4WML extends AbstractCatalogServlet {
    /** Constructor */
    public CatalogServlet4WML() {
        super();
    }
    
    // get the content type returned
    protected String getContentType() {
        return "text/vnd.wap.wml";
    }
    // get the stylesheet (url)
    protected String getStylesheetUrl() {
        return "/catalog_wml.xsl";
    }
    
}
