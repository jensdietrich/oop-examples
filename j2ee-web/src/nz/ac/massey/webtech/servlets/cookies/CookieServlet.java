package nz.ac.massey.webtech.servlets.cookies;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 * Servlet showing how to work with coockies.
 * @author  Jens Dietrich   
 * @version 1.0
 */
public class CookieServlet extends HttpServlet {
    
    /** Initializes the servlet.
     */
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        
    }
    
    /** Destroys the servlet.
     */
    public void destroy() {
        
    }
    
    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
       
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Cookie Servlet</title>");
        out.println("</head>");
        out.println("<body>");
         
        out.println("<h1>Cookie Example</h1>");
        
        // set coockie from submitted value if there is one
        String newCookieValue = request.getParameter("name");
        if (newCookieValue!=null && newCookieValue.trim().length()==0) newCookieValue=null;
        if (newCookieValue!=null) {
            // set cookie !
            Cookie newCookie = new Cookie("name",newCookieValue);
            newCookie.setMaxAge(60*60*24*365);
            response.addCookie(newCookie);
        }
        // find cookie
        String cookieValue = newCookieValue;
        Cookie[] cookies = request.getCookies();
        if (cookieValue==null && cookies!=null && cookies.length>0) {
            // we assume here that we only set one cookie !
            cookieValue = cookies[0].getValue();
        }
        // print value
        if (cookieValue!=null) {
            out.print(newCookieValue==null?"Welcome back ":"Thank you for registering ");
            out.print("<strong>");
            out.println(cookieValue);
            out.println("</strong><hr>");
            out.print("If you are not <strong>");
            out.print(cookieValue);
            out.println("</strong> you can enter your correct name here: <p>");
        }
        else out.println("Please enter your name here<p>");
        
        // build form
        out.println("<form>");
        out.println("<input name=\"name\" size=60>");
        out.println("<input type=submit value=\"OK\">");
        out.println("</form>");
        
        
        out.println("</body>");
        out.println("</html>");
        
        out.close();
    }
    
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    
}
