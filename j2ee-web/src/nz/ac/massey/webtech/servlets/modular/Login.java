package nz.ac.massey.webtech.servlets.modular;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 * The login - to be included by the main servlet.
 * @author  Jens Dietrich   
 * @version 1.0
 */
public class Login extends HttpServlet {
    public static final String PASSWORD = "42";
    /** Initializes the servlet.
     */
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        
    }
    
    /** Destroys the servlet.
     */
    public void destroy() {
        
    }
    
    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        
        
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        boolean loginOK = session.getAttribute("login")!=null;
        
        if (!loginOK) {
            // check whether the user just logged in
            String passwd = request.getParameter("password");
            if (PASSWORD.equals(passwd)) {
                loginOK = true;
                session.setAttribute("login", "true");
            }
        }
        
        if (loginOK) 
            getServletContext().getRequestDispatcher("/ForwardInclude").forward(request,response);
        
        // else print login form 
        response.setContentType("text/html");
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Modular Application using Include and Forward</title>");
        out.println("</head>");
        out.println("<body>");
        out.println("<h1>Login</h1>");
        out.println("hint: the password is 42<p>");
        out.println("<form>");
        out.println("<input type=password name=\"password\" size=30>");
        out.println("<input type=submit value=\"login\">");
        out.println("<form>");
        out.print("</form>");
        
        out.println("</body></html>");
        
    }
    
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    /**
     * Handle an exception.
     */
    private void handleException(String msg,Throwable t,PrintWriter out) throws ServletException, IOException {
        out.print(msg);
        out.println("<p><hr>");
        getServletContext().log(msg,t);
        throw new ServletException(msg,t);
    } 
    
}
