package nz.ac.massey.webtech.servlets.modular;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 * The main servlet in an application that uses forward and include.
 * @author  Jens Dietrich   
 * @version 1.0
 */
public class ApplicationServlet extends HttpServlet {
    
    /** Initializes the servlet.
     */
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        
    }
    
    /** Destroys the servlet.
     */
    public void destroy() {
        
    }
    
    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
       
        out.println("<html>");
        out.println("<head>");
        out.println("<title>Modular Application using Include and Forward</title>");
        out.println("</head>");
        out.println("<body>");
        
        // display header - use static page
        this.getServletContext().getRequestDispatcher("/example8/header.html").include(request,response);
        
        // body 
        String counterAsString = (String)session.getAttribute("counter");
        if (counterAsString==null) counterAsString="0";
        int counter = Integer.parseInt(counterAsString);
        out.print("The current server time is: ");
        out.println(new java.util.Date());
        out.println("<p>");
        
        out.print("Click <a href=\"login\">here</a> to continue!");
        
        // display footer - use dynamic page
        this.getServletContext().getRequestDispatcher("/footer").include(request,response);
        
        out.println("</body>");
        out.println("</html>");
        
        out.close();
    }
    
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    /**
     * Handle an exception.
     */
    private void handleException(String msg,Throwable t,PrintWriter out) throws ServletException, IOException {
        out.print(msg);
        out.println("<p><hr>");
        getServletContext().log(msg,t);
        throw new ServletException(msg,t);
    } 
    
}
