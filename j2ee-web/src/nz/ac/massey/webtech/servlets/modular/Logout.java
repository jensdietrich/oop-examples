package nz.ac.massey.webtech.servlets.modular;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;

/**
 * The logout.
 * @author  Jens Dietrich   
 * @version 1.0
 */
public class Logout extends HttpServlet {

    /** Initializes the servlet.
     */
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        
    }
    
    /** Destroys the servlet.
     */
    public void destroy() {
        
    }
    
    /** Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        
        HttpSession session = request.getSession();
        session.removeAttribute("login");
        session.removeAttribute("counter");
        session.invalidate();
        getServletContext().getRequestDispatcher("/login").forward(request,response);
    }
    
    /** Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }
    
    /** Returns a short description of the servlet.
     */
    public String getServletInfo() {
        return "Short description";
    }
    /**
     * Handle an exception.
     */
    private void handleException(String msg,Throwable t,PrintWriter out) throws ServletException, IOException {
        out.print(msg);
        out.println("<p><hr>");
        getServletContext().log(msg,t);
        throw new ServletException(msg,t);
    } 
    
}
