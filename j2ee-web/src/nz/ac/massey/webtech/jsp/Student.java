
package nz.ac.massey.webtech.jsp;

/**
 * A simple bean (to be used as model by a JSP page). 
 * @author  Jens Dietrich   
 * @version 1.0
 */
public class Student {
    
    private String name = null;
    private String firstName = null;
    private int age = 0;
    
    /** Creates a new instance of Student */
    public Student() {
    }
    
    /** Getter for property age.
     * @return Value of property age.
     *
     */
    public int getAge() {
        return age;
    }
    
    /** Setter for property age.
     * @param age New value of property age.
     *
     */
    public void setAge(int age) {
        this.age = age;
    }
    
    /** Getter for property firstName.
     * @return Value of property firstName.
     *
     */
    public java.lang.String getFirstName() {
        return firstName;
    }
    
    /** Setter for property firstName.
     * @param firstName New value of property firstName.
     *
     */
    public void setFirstName(java.lang.String firstName) {
        this.firstName = firstName;
    }
    
    /** Getter for property name.
     * @return Value of property name.
     *
     */
    public java.lang.String getName() {
        return name;
    }
    
    /** Setter for property name.
     * @param name New value of property name.
     *
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }
    
}
