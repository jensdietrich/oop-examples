delimiter $$

CREATE TABLE `catalog` (
  `cd` varchar(30) NOT NULL,
  `title` varchar(45) DEFAULT NULL,
  `artist` varchar(45) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `company` varchar(45) DEFAULT NULL,
  `price` varchar(15) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  PRIMARY KEY (`cd`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1$$

INSERT INTO `test`.`catalog`
(`cd`,
`title`,
`artist`,
`country`,
`company`,
`price`,
`year`)
VALUES
(
{cd: VARCHAR},
{title: VARCHAR},
{artist: VARCHAR},
{country: VARCHAR},
{company: VARCHAR},
{price: VARCHAR},
{year: INT}
);
