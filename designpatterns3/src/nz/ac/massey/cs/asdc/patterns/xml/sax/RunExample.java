package nz.ac.massey.cs.asdc.patterns.xml.sax;

import org.xml.sax.*;
import org.xml.sax.helpers.*;

/**
 * Simple example showing how to use the Xerces SAX Parser.
 * @author Jens Dietrich
 */
public class RunExample {
	/**
	 * Main method - run example.
	 * @param args an array of runtime arguments
	 */
	public static void main(String[] args) throws Exception {
		
		InputSource input = new InputSource("email1.xml");
		
		// initialize parser
		XMLReader reader = XMLReaderFactory.createXMLReader("org.apache.xerces.parsers.SAXParser");
		
		// set handler (event listeners)
		reader.setContentHandler(new ExtractRecipients());
		reader.setErrorHandler(new DefaultHandler());
		
		
		// read document
		reader.parse(input);
	}

}
