package nz.ac.massey.cs.asdc.patterns.xml.xpath;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;
import org.xml.sax.InputSource;

public class RunExample {
	public static void main(String[] args) throws Exception {
		XPathFactory  factory=XPathFactory.newInstance();
		XPath xPath=factory.newXPath();
		InputSource input = new InputSource("email1.xml");
		
		System.out.println(xPath.evaluate("/email/to[1]/email_address", input)) ;
		System.out.println(xPath.evaluate("/email/to[2]/email_address", input)) ;
		// remark:
		// can also query for nodes: NodeList nodes = (NodeList)expr.evaluate(input, XPathConstants.NODESET);

	}
}
