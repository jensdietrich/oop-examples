package nz.ac.massey.cs.asdc.patterns.xml.dom;

import java.io.FileOutputStream;
import java.util.Date;
import org.jdom.*;
import org.jdom.output.XMLOutputter;
/**
 * Simple example showing how to use JDOM to create a document programmatically.
 * @author Jens Dietrich
 */
public class JDomCreateDocExample {
	/**
	 * Main method - run example.
	 * @param args an array of runtime arguments
	 */
	public static void main(String[] args)  {
		
		try {
			// build JDOM document
			Document doc = new Document();
			
			// comment
			doc.addContent(new Comment("Creates by JDOM " + new Date()));
			
			// content
			Element html = new Element("html");
			Element body = new Element("body");
			body.setAttribute("bgcolor","#CCCCCC");
			Element title = new Element("h1");
			title.addContent("Hello World");
			body.addContent(title);
			body.addContent("This is some text");
			html.addContent(body);
			doc.setRootElement(html);
					
			// write document to file
			XMLOutputter outputter = new XMLOutputter();
			outputter.output(doc,new FileOutputStream("test.xml"));
			
		}
		catch (Exception x) {
			x.printStackTrace();
		}
		finally {
			System.exit(0);
		}
	
	} 

}
