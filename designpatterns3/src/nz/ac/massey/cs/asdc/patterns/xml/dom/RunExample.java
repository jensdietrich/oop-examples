package nz.ac.massey.cs.asdc.patterns.xml.dom;

import java.io.FileInputStream;
import org.jdom.*;
import org.jdom.input.SAXBuilder;
/**
 * Simple example showing how to use JDOM to parse a document.
 * @author Jens Dietrich
 */
public class RunExample {

	public static void main(String[] args) throws Exception {
		
		
		// build JDOM document
		SAXBuilder builder = new SAXBuilder();
		
		Document doc = builder.build(new FileInputStream("email1.xml"));
		
		// print tree skeleton to show how to work with the API
		// print(doc.getRootElement(),0);	
		
		Element e = doc.getRootElement();
		
		// iterate over receivers of the email - navigate through the DOM
		for (Object child:e.getChildren("to")) {
			Element eTo = (Element)child;
			System.out.print("to: ");
			System.out.print(eTo.getChildText("display_name"));
			System.out.print(" <");
			System.out.print(eTo.getChildText("email_address"));
			System.out.println(">");
		}
	}


}
